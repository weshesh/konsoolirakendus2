﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Palgakalkulaator 1000
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("-------------------------------------------");
            Console.WriteLine("---------------$$--$$----------------------");
            Console.WriteLine("------------$$$$$$$$$$$$-------------------");
            Console.WriteLine("----------$$$$-$$--$$-$$$$-----------------");
            Console.WriteLine("--------$$$$---$$--$$---$$$----------------");
            Console.WriteLine("--------$$$----$$--$$-----$----------------");
            Console.WriteLine("--------$$$$---$$--$$----------------------");
            Console.WriteLine("----------$$$$-$$--$$----------------------");
            Console.WriteLine("-------------$$$$$-$$----------------------");
            Console.WriteLine("---------------$$$$$$$$--------------------");
            Console.WriteLine("---------------$$--$$-$$$$-----------------");
            Console.WriteLine("---------------$$--$$---$$$$---------------");
            Console.WriteLine("---------$-----$$--$$----$$$---------------");
            Console.WriteLine("---------$$$---$$--$$---$$$$---------------");
            Console.WriteLine("----------$$$$-$$--$$-$$$$-----------------");
            Console.WriteLine("------------$$$$$$$$$$$--------------------");
            Console.WriteLine("---------------$$--$$----------------------");
            Console.WriteLine("-------------------------------------------");

            Console.WriteLine("Kui suur on sinu palk (Bruto)?");
            double palk = double.Parse(Console.ReadLine());
            double sotsiaalMaks = palk * 0.33;
            double tKindlustusmakse = palk * 0.008;
            double palgafond = palk + sotsiaalMaks + tKindlustusmakse;
            double kogumispension = palk * 0.02;
            double tkmt = palk * 0.016;
            double maksuvabaTulu = 500;
            double tulumaks = palk - maksuvabaTulu - kogumispension - tkmt;
            tulumaks *= 0.2;
            double netopalk = palk - kogumispension - tkmt - tulumaks;


            Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            Console.WriteLine("Palgafond: " + palgafond);
            Console.WriteLine("Sotsiaalmaks: " + sotsiaalMaks);
            Console.WriteLine("Töötuskindlustusmakse(tööandja): " + tKindlustusmakse);
            Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            Console.WriteLine("");
            Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            Console.WriteLine("Brutopalk: " + palk);
            Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            Console.WriteLine("");
            Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            Console.WriteLine("Kogumispension (II sammas): "+kogumispension);
            Console.WriteLine("Töötuskindlustusmakse(töötaja): "+tkmt);
            Console.WriteLine("Tulumaks: "+tulumaks);
            Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            Console.WriteLine("");
            Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            Console.WriteLine("Netopalk: "+netopalk);
            Console.WriteLine("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$");

            string hinnang = "Sinu palk " + (palk < 100000 ? "...noh võiks ikka 100 tuhat olla!" : "on suht normaalne.");
            Console.WriteLine(hinnang);
        }
    }
}
